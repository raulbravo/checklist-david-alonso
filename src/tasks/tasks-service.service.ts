import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Task } from 'src/models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  url: string = "https://checklistbackend.azurewebsites.net/api/tasks";
  tasks: Task[];

  constructor(private http: HttpClient) { }



  getAllTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(this.url);
  }

  async addTask(taskText: string, taskTags: string[], taskCategoryID: string): Promise<Task> {
    return await this.http.post<Task>(this.url, { text: taskText, sortNumber: 0, categoryId: taskCategoryID, tags: taskTags }).toPromise();
  }

  async deleteTask(idTask: string): Promise<any> {
    let endPoints = "/{" + idTask + "}";
    return await this.http.delete(this.url + endPoints).toPromise();
  }

  async editTask(idTask: string, textTask: string, idCategory: string, tagList: string[], sortNumber: number): Promise<any> {
    let endPoint = this.url + "/" + idTask;

    return await this.http.patch<any>(endPoint, [{ "op": "replace", "path": "text", "value": textTask },
    { "op": "replace", "path": "sortNumber", "value": sortNumber },
    { "op": "replace", "path": "categoryId", "value": idCategory },
    { "op": "replace", "path": "tags", "value": tagList }
    ]).toPromise();
  }

  async updateDone(task: Task): Promise<any> {
    let endPoint = this.url + "/" + task.id;
    if (task.done == true) {
      task.done = false;
    } else {
      task.done = true;
    }
    return await this.http.patch<any>(endPoint, [{ "op": "replace", "path": "done", "value": task.done }]).toPromise();
  }



}
