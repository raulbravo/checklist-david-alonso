import { Component, Input, OnInit, Output } from '@angular/core';
import { TasksService } from 'src/tasks/tasks-service.service';
import { Task } from 'src/models/task.model';
import { CommonService } from '../common.service';
import { SubtasksService } from 'src/subtasks/subtasks-services.service';
import { SubTask } from 'src/models/subtask.model';

@Component({
  selector: 'app-individualCardTask',
  templateUrl: './individualCardTask.component.html',
  styleUrls: ['./individualCardTask.component.css']
})
export class IndividualCardTaskComponent implements OnInit {
  @Input() taskElement: Task;
  public isCollapsed = true;

  constructor(public tasksService: TasksService,
    public commonService: CommonService,
    public subtaskService: SubtasksService) { }

  ngOnInit() {
  }

  async deleteTask(TaskElementF: Task) {
    let indexDeleteElement: number = -1;
    this.tasksService.tasks.forEach((task, i) => {
      if (task.id === TaskElementF.id) {
        indexDeleteElement = i;
      }
    })
    try {
      await this.tasksService.deleteTask(this.tasksService.tasks[indexDeleteElement].id);
      this.tasksService.tasks.splice(indexDeleteElement, 1);
    } catch (error) {
      console.error(error);
    }
    this.tasksService.tasks = [...this.tasksService.tasks];
  }

  changeCheckValueTask(task: Task) {
    this.tasksService.updateDone(task);
    this.tasksService.tasks = [...this.tasksService.tasks];
  }

  showModal(type: string, data: any) {
    this.commonService.showModal$.next({ modalName: type, modalData: data });
  }

  deleteSubTask(task: Task, subtask: SubTask) {
    let indexTask: number = -1;
    let indexSubtask: number = -1;

    this.tasksService.tasks.forEach((taskElement, i) => {
      if (taskElement.id === task.id) {
        indexTask = i;
        taskElement.subTasks.forEach((subtaskElement, j) => {
          if (subtask.id === subtaskElement.id) {
            indexSubtask = j;
          }
        })
      }
    });

    this.subtaskService.deleteSubtask(this.tasksService.tasks[indexTask].id, this.tasksService.tasks[indexTask].subTasks[indexSubtask].id);
    this.tasksService.tasks[indexTask].subTasks.splice(indexSubtask, 1);
  }
}
