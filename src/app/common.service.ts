import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  searchByText: string;
  tagSelect: string = "";
  categorySelected: string = "";
  checkShow: boolean = true;
  typeTableOrCards:boolean = true;
  showModal$: Subject<modalInformation> = new Subject<modalInformation>();

  constructor(public modalService: NgbModal) { }
}

export interface modalInformation {
  modalName: string;
  modalData?: any;
}