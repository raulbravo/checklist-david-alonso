import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/categories/categories-service.service';
import { TagsService } from 'src/tags/tags-service.service';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-filtersTable',
  templateUrl: './filtersTable.component.html',
  styleUrls: ['./filtersTable.component.css']
})
export class FiltersTableComponent implements OnInit {

  constructor(public commonService:CommonService,
    public categoriesService:CategoriesService,
    public tagsService:TagsService) { }

  ngOnInit() {
  }

  changeValueDoneTasks(): void {
    if (this.commonService.checkShow == true) {
      this.commonService.checkShow = false;
    } else {
      this.commonService.checkShow = true;
    }
  }
}
