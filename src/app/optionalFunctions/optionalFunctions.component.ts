import { Component, OnInit } from '@angular/core';
import { Tag } from 'src/models/tag.model';
import { TasksService } from 'src/tasks/tasks-service.service';
declare var require: any;

@Component({
  selector: 'app-optionalFunctions',
  templateUrl: './optionalFunctions.component.html',
  styleUrls: ['./optionalFunctions.component.css']
})
export class OptionalFunctionsComponent implements OnInit {

  constructor(public tasksService:TasksService) { }

  ngOnInit() {
  }

  
  exportToExcel() {
    const XLSX = require('xlsx');
    let tasksDataCsv = [];

    for (let item of this.tasksService.tasks) {
      tasksDataCsv.push(
        {
          Category: item.category.name,
          Task: item.text,
          Subtasks: "",
          Tags: this.tagsToString(item.tags),
          Done: item.done
        }
      )
      if (item.subTasks.length > 0) {
        for (let itemSubtask of item.subTasks) {
          tasksDataCsv.push({
            Category: item.category.name,
            Task: item.text,
            Subtasks: itemSubtask.text,
            Tags: "",
            Done: itemSubtask.done
          })
        }
      }
    }

    let csvData = tasksDataCsv;
    let csvWS = XLSX.utils.json_to_sheet(csvData);
    // Create a new Workbook
    var wb = XLSX.utils.book_new()
    // Name sheet
    XLSX.utils.book_append_sheet(wb, csvWS, 'Task List')
    // Export excel with date
    var date = new Date();
    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    var today = dd + mm + yyyy;
    XLSX.writeFile(wb, 'taskList_' + today + '.xlsx');
  }

  tagsToString(arrayTags: Tag[]): string {
    let stringTags = "";
    arrayTags.forEach((elementTag) => {
      stringTags = stringTags + " " + elementTag.name + ",";
    });
    stringTags = stringTags.substring(0, stringTags.length - 1);
    return stringTags;
  }

  markAllTaskAsDone() {
    if (this.tasksService.tasks) {
      this.tasksService.tasks.forEach(taskElement => {
        if (taskElement.done === false) {
          this.tasksService.updateDone(taskElement);
        }
      });
      this.tasksService.tasks = [...this.tasksService.tasks];
    }
  }

  markAllTaskAsNotDone() {
    if (this.tasksService.tasks) {
      this.tasksService.tasks.forEach(taskElement => {
        if (taskElement.done === true) {
          this.tasksService.updateDone(taskElement);
        }
      });
      this.tasksService.tasks = [...this.tasksService.tasks];
    }
  }

  deleteAllTasks() {
    if (this.tasksService.tasks) {
      this.tasksService.tasks.forEach((taskElement) => {
        this.tasksService.deleteTask(taskElement.id);
      });
      this.tasksService.tasks = [...this.tasksService.tasks];
    }
  }

}
