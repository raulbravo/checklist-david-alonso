import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoriesService } from 'src/categories/categories-service.service';
import { SubtasksService } from 'src/subtasks/subtasks-services.service';
import { TagsService } from 'src/tags/tags-service.service';
import { TasksService } from 'src/tasks/tasks-service.service';
import { CommonService } from './common.service';
import { AddTaskModalComponent } from 'src/modals/addTaskModal/addTaskModal.component';
import { CategoriesModalComponent } from 'src/modals/categoriesModal/categoriesModal.component';
import { EditCategoryModalComponent } from 'src/modals/editCategoryModal/editCategoryModal.component';
import { TagsModalComponent } from 'src/modals/tagsModal/tagsModal.component';
import { EditTaskModalComponent } from 'src/modals/editTaskModal/editTaskModal.component';
import { AddSubtaskModalComponent } from 'src/modals/addSubtaskModal/addSubtaskModal.component';
import { EditSubTaskModalComponent } from 'src/modals/editSubTaskModal/editSubTaskModal.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {

  @ViewChild('addTaskModal') addTaskModal: AddTaskModalComponent;
  @ViewChild('categoriesModal') categoriesModal: CategoriesModalComponent;
  @ViewChild('editCategoryModal') editCategoryModal: EditCategoryModalComponent;
  @ViewChild('tagsModal') tagsModal: TagsModalComponent;
  @ViewChild('editTaskModal') editTaskModal: EditTaskModalComponent;
  @ViewChild('addSubTaskModal') addSubTaskModal: AddSubtaskModalComponent;
  @ViewChild('editSubTaskModal') editSubTaskModal: EditSubTaskModalComponent;

  constructor(
    public categoriesService: CategoriesService,
    public tasksService: TasksService,
    public tagsService: TagsService,
    public subtaskService: SubtasksService,
    public commonService: CommonService) {
    this.getTasks();
    this.getCategories();
    this.getTags();
  }

  ngAfterViewInit(): void {
  }

  ngOnInit() {
    this.commonService.showModal$.subscribe(modalInformation => {
      this.showContent(modalInformation.modalName, modalInformation.modalData);
    })
  }

  showContent(type: string, data: any) {
    switch (type) {
      case "showModalAddTask": {
        this.addTaskModal.open1();
        break;
      }
      case "showModalCategories": {
        this.categoriesModal.openCategories();
        break;
      }
      case "showModalEditCategory": {
        this.editCategoryModal.openEditCategory(data);
        break;
      }
      case "showModalAddSubTask": {
        this.addSubTaskModal.openAddSubTask(data);
        break;
      }
      case "showModalEditTask": {
        this.editTaskModal.openEditTask(data);
        break;
      }
      case "showModalEditSubTask": {
        this.editSubTaskModal.openEditSubTask(data);
        break;
      }
      case "showModalTags": {
        this.tagsModal.openTags();
        break;
      }
    }
  }

 async  getCategories(): Promise<any> {   
    this.categoriesService.categories = await this.categoriesService.getAllCategories().toPromise();
    this.categoriesService.categories.sort((function(a, b) {
      var textA = a.name.toUpperCase();
      var textB = b.name.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
  }));
  }

  getTasks(): void {
    this.tasksService.getAllTasks().subscribe(res => {
      this.tasksService.tasks = res;
    })
  }

  async getTags() {
    this.tagsService.tags = await this.tagsService.getAllTags().toPromise();
    this.tagsService.tags.sort((function(a, b) {
      var textA = a.name.toUpperCase();
      var textB = b.name.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
  }));
  }

}