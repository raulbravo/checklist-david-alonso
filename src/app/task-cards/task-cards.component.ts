import { Component, Input, OnInit } from '@angular/core';
import { TasksService } from 'src/tasks/tasks-service.service';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-task-cards',
  templateUrl: './task-cards.component.html',
  styleUrls: ['./task-cards.component.css']
})
export class TaskCardsComponent implements OnInit {
  constructor(public tasksService:TasksService,
    public commonService:CommonService) { }

  ngOnInit() {
  }
  getToDo(): number {
    var todoTasks = 0;
    if (this.tasksService.tasks && this.tasksService.tasks.length != undefined) {
      if (this.tasksService.tasks.length > 0) {

        this.tasksService.tasks.forEach((task) => {
          if (task.done == false) {
            todoTasks++;
          }
        });
      }
    }
    return todoTasks;
  }

  getDone(): number {
    var doneTasks = 0;
    if (this.tasksService.tasks && this.tasksService.tasks.length != undefined) {
      if (this.tasksService.tasks.length > 0) {

        this.tasksService.tasks.forEach((task) => {
          if (task.done == true) {
            doneTasks++;
          }
        });
      }
    }
    return doneTasks;
  }

  clearFilters() {
    this.commonService.checkShow = true;
    this.commonService.searchByText = "";
    this.commonService.tagSelect = "";
    this.commonService.categorySelected = "";
  }

  
}
