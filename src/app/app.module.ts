import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FilterArrayPipe } from 'src/pipes/filter-array.pipe';
import { FilterSubtasksPipe } from 'src/pipes/filter-subtasks.pipe';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { TaskTableComponent } from './task-table/task-table.component';
import { AddTaskModalComponent } from 'src/modals/addTaskModal/addTaskModal.component';
import { CategoriesModalComponent } from 'src/modals/categoriesModal/categoriesModal.component';
import { EditCategoryModalComponent } from 'src/modals/editCategoryModal/editCategoryModal.component';
import { TagsModalComponent } from 'src/modals/tagsModal/tagsModal.component';
import { EditTaskModalComponent } from 'src/modals/editTaskModal/editTaskModal.component';
import { AddSubtaskModalComponent } from 'src/modals/addSubtaskModal/addSubtaskModal.component';
import { EditSubTaskModalComponent } from 'src/modals/editSubTaskModal/editSubTaskModal.component';
import { OptionalFunctionsComponent } from './optionalFunctions/optionalFunctions.component';
import { FiltersTableComponent } from './filtersTable/filtersTable.component';
import { TaskCardsComponent } from './task-cards/task-cards.component';
import { IndividualCardTaskComponent } from './individualCardTask/individualCardTask.component';

@NgModule({
  declarations: [
    AppComponent,
    FilterArrayPipe,
    FilterSubtasksPipe,
    ToolbarComponent,
    TaskTableComponent,
    AddTaskModalComponent,
    CategoriesModalComponent,
    EditCategoryModalComponent,
    TagsModalComponent,
    EditTaskModalComponent,
    AddSubtaskModalComponent,
    EditSubTaskModalComponent,
    OptionalFunctionsComponent,
    FiltersTableComponent,
    TaskCardsComponent,
    IndividualCardTaskComponent
  ],
  imports: [
    BrowserModule,
    BsDropdownModule.forRoot(),
    BrowserAnimationsModule,
    FormsModule,
    NgbModule,
    HttpClientModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }