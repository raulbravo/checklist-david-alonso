import { Component, Input, OnInit } from '@angular/core';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  constructor(public commonService: CommonService) { }

  ngOnInit(): void {
  }

  showModal(type: string) {
    this.commonService.showModal$.next({ modalName: type });
  }

  changeType(){
    if(this.commonService.typeTableOrCards){
      this.commonService.typeTableOrCards = false;
    }else{
      this.commonService.typeTableOrCards = true;
    }
  }


}
