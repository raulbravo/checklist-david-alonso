import { Component, Input, OnInit } from '@angular/core';
import { SubTask } from 'src/models/subtask.model';
import { SubtasksService } from 'src/subtasks/subtasks-services.service';
import { TasksService } from 'src/tasks/tasks-service.service';
import { CommonService } from '../common.service';
import { Task } from 'src/models/task.model';

@Component({
  selector: 'app-task-table',
  templateUrl: './task-table.component.html',
  styleUrls: ['./task-table.component.css']
})
export class TaskTableComponent implements OnInit {

  constructor(public commonService: CommonService,
    public tasksService: TasksService,
    public subtaskService: SubtasksService) { }

  ngOnInit(): void {

  }

  showModal(type: string, data: any) {
    this.commonService.showModal$.next({ modalName: type, modalData: data });
  }

  async deleteTask(indexDeleteElement: number) {
    try {
      await this.tasksService.deleteTask(this.tasksService.tasks[indexDeleteElement].id);
      this.tasksService.tasks.splice(indexDeleteElement, 1);
    } catch (error) {
      console.error(error);
    }
    this.tasksService.tasks = [...this.tasksService.tasks];
  }

  changeCheckValueTask(task: Task) {
    this.tasksService.updateDone(task);
    this.tasksService.tasks = [...this.tasksService.tasks];
  }

  changeCheckValueSubTask(subtask: SubTask, idTask: string) {
    this.subtaskService.updateDone(subtask, idTask);
  }

  deleteSubTask(indexTask: number, indexSubtask: number) {
    this.subtaskService.deleteSubtask(this.tasksService.tasks[indexTask].id, this.tasksService.tasks[indexTask].subTasks[indexSubtask].id);
    this.tasksService.tasks[indexTask].subTasks.splice(indexSubtask, 1);
  }

  getToDo(): number {
    var todoTasks = 0;
    if (this.tasksService.tasks && this.tasksService.tasks.length != undefined) {
      if (this.tasksService.tasks.length > 0) {

        this.tasksService.tasks.forEach((task) => {
          if (task.done == false) {
            todoTasks++;
          }
        });
      }
    }
    return todoTasks;
  }

  getDone(): number {
    var doneTasks = 0;
    if (this.tasksService.tasks && this.tasksService.tasks.length != undefined) {
      if (this.tasksService.tasks.length > 0) {

        this.tasksService.tasks.forEach((task) => {
          if (task.done == true) {
            doneTasks++;
          }
        });
      }
    }
    return doneTasks;
  }

  clearFilters() {
    this.commonService.checkShow = true;
    this.commonService.searchByText = "";
    this.commonService.tagSelect = "";
    this.commonService.categorySelected = "";
  }

}
