import { Category } from "./category.model";
import { SubTask } from "./subtask.model";
import { Tag } from "./tag.model";

export class Task{
    id:string;
    text:String;
    category:Category;
    tags:Tag[];
    subTasks:SubTask[];
    done:boolean;
    sortNumber:number;

}