export class SubTask{
    id:string;
    text:string;
    done:boolean;
    sortNumber:number;
}