import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from 'src/models/category.model';


@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  categories: Category[] = [];
  url: string = "https://checklistbackend.azurewebsites.net/api/categories";

  constructor(private http: HttpClient) {


  }

  getAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.url);
  }

  addCategory(categoryName: string): Observable<Category> {

    return this.http.post<Category>(this.url, { name: '' + categoryName });
  }

  async deleteCategory(idCategory: string): Promise<any> {
    let endPoints = "/" + idCategory;
    return await this.http.delete(this.url + endPoints).toPromise();
  }


  editCategory(idCategory: string, updatedValue: string): void {
    let endPoint = this.url +"/"+idCategory ;
    this.http.patch(endPoint, [{ "op": "replace", "path": "name", "value": updatedValue }]).subscribe(res=>{

    });
  }
}


