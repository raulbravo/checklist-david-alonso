import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TagsService } from 'src/tags/tags-service.service';

@Component({
  selector: 'app-tagsModal',
  templateUrl: './tagsModal.component.html',
  styleUrls: ['./tagsModal.component.css']
})
export class TagsModalComponent implements OnInit {
  @ViewChild('tagsContent') contentTags: any;
  closeResult: string;

  constructor(public modal: NgbModal,
    public tagsService: TagsService) {

  }

  ngOnInit() {
  }

  openTags() {
    this.modal.open(this.contentTags).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
    });
  }

  async deleteTag(indexTag: number) {
    await this.tagsService.deleteTag(this.tagsService.tags[indexTag].id);
    this.tagsService.tags.splice(indexTag, 1);

  }
}
