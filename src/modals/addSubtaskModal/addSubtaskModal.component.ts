import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SubTask } from 'src/models/subtask.model';
import { SubtasksService } from 'src/subtasks/subtasks-services.service';
import { TasksService } from 'src/tasks/tasks-service.service';

@Component({
  selector: 'app-addSubtaskModal',
  templateUrl: './addSubtaskModal.component.html',
  styleUrls: ['./addSubtaskModal.component.css']
})
export class AddSubtaskModalComponent implements OnInit {
  @ViewChild('addSubtaskModal') addSubtaskModal: any;
  inputSubTaskSortNumber: number = 0;
  inputSubTaskText: string = "";
  constructor(public modal: NgbModal,
    public subtaskService:SubtasksService,
    public tasksService:TasksService) { }

  ngOnInit() {
  }

  openAddSubTask(data: any) {
    this.modal.open(this.addSubtaskModal).result.then((result) => {
      if (result === 'Add subTask') {
        this.postSubTask(data.id);
      }
    }, (reason) => {
    });
  }

  postSubTask(idTask: string) {
    this.subtaskService.addSubTask(idTask, this.inputSubTaskText, this.inputSubTaskSortNumber).subscribe(subtask => this.pushSubtaskInTask(idTask, subtask));
  }

  pushSubtaskInTask(idTask: String, subtask: SubTask): void {
    this.tasksService.tasks.forEach((task) => {
      if (task.id === idTask) {
        task.subTasks.push(subtask);
        this.inputSubTaskText = "";
      }
    });
  }
}
