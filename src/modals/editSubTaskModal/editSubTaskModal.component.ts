import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SubtasksService } from 'src/subtasks/subtasks-services.service';
import { TasksService } from 'src/tasks/tasks-service.service';

@Component({
  selector: 'app-editSubTaskModal',
  templateUrl: './editSubTaskModal.component.html',
  styleUrls: ['./editSubTaskModal.component.css']
})
export class EditSubTaskModalComponent implements OnInit {
  @ViewChild('editSubTaskContent') editSubTaskContent: any;
  inputEditSubtask: string = "";
  inputEditSubTaskSortNumber: number = 0;

  constructor(public modal: NgbModal,
    public tasksService:TasksService,
    public subtaskService:SubtasksService) { }

  ngOnInit() {
  }

  openEditSubTask(data: any) {
    this.inputEditSubtask = data.text;
    this.inputEditSubTaskSortNumber = data.sortNumber;
    this.modal.open(this.editSubTaskContent).result.then((result) => {
      if (result === 'Save edit subtask') {
        this.editsubtask(data.id);
      }
    }, (reason) => {
    });
  }

  async editsubtask(idSubTask: string) {
    try {
      await this.subtaskService.editSubTask(idSubTask, this.inputEditSubtask, this.inputEditSubTaskSortNumber);

    } catch (error) {
      console.log(error);
    }
    this.tasksService.tasks.forEach(task => {
      task.subTasks.forEach(subtask => {
        if (subtask.id === idSubTask) {
          subtask.text = this.inputEditSubtask;
          subtask.sortNumber = this.inputEditSubTaskSortNumber;
        }
      })
    });

  }
}
