import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoriesService } from 'src/categories/categories-service.service';
import { Tag } from 'src/models/tag.model';
import { TagsService } from 'src/tags/tags-service.service';
import { TasksService } from 'src/tasks/tasks-service.service';

@Component({
  selector: 'app-editTaskModal',
  templateUrl: './editTaskModal.component.html',
  styleUrls: ['./editTaskModal.component.css']
})
export class EditTaskModalComponent implements OnInit {
  @ViewChild('editTaskContent') editTaskContent: any;
  inputEditTask: string = "";
  inputEditTaskCategory: string = "";
  inputEditTaskSortNumber: number = 0;
  inputTagEditTask: string = "";
  inputEditTaskTags: Tag[] = [];

  constructor(public modal: NgbModal,
    public tasksService: TasksService,
    public categoriesService: CategoriesService,
    public tagsService: TagsService) { }

  ngOnInit() {
  }

  openEditTask(data: any) {
    this.inputEditTask = data.text;
    this.inputEditTaskTags = data.tags;
    this.inputEditTaskCategory = data.category.id;
    this.inputEditTaskSortNumber = data.sortNumber;
    this.modal.open(this.editTaskContent).result.then((result) => {
      if (result === 'Save edit task') {
        this.editTask(data.id);
      }
    }, (reason) => {
    });
  }

  async addEditTaskTag(nameTag: string) {
    let flag: boolean = false;
    this.inputEditTaskTags.forEach((tag) => {
      if (tag.name.toLowerCase() == nameTag.toLowerCase()) {
        flag = true;
      }
    });
    if (flag == false) {
      this.tagsService.tags.forEach((tag) => {
        if (tag.name.toLowerCase() == nameTag.toLowerCase()) {
          this.inputEditTaskTags.push(tag);
          flag = true;
        }
      });
      if (flag == false) {
        this.inputEditTaskTags.push(await this.tagsService.addTag(nameTag));

      }
    }
    this.inputTagEditTask = "";
  }

  deleteEditTaskTag(index: number) {
    this.inputEditTaskTags.splice(index, 1);
  }

  async editTask(idTask: string) {
    let arrayIdTags: string[] = [];
    this.inputEditTaskTags.forEach((tag) => {
      arrayIdTags.push(tag.id);
    });

    try {
      await this.tasksService.editTask(idTask, this.inputEditTask, this.inputEditTaskCategory, arrayIdTags, this.inputEditTaskSortNumber);

    } catch (error) {
      console.log(error);
    }

    this.tasksService.tasks.forEach(task => {
      if (task.id === idTask) {
        task.text = this.inputEditTask;
        task.category = this.categoriesService.categories.filter(category => category.id === this.inputEditTaskCategory)[0];
        task.sortNumber = this.inputEditTaskSortNumber;
        task.tags = this.inputEditTaskTags;
      }
    });
    this.tasksService.tasks = [...this.tasksService.tasks];
  }

}
