import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/common.service';
import { CategoriesService } from 'src/categories/categories-service.service';

@Component({
  selector: 'app-categoriesModal',
  templateUrl: './categoriesModal.component.html',
  styleUrls: ['./categoriesModal.component.css']
})
export class CategoriesModalComponent implements OnInit {
  @ViewChild('categoriesContent') contentCategories: any;
  closeResult: string;
  inputCategoryName: string = "";


  constructor(public modal: NgbModal,
    public categoriesService: CategoriesService,
    public commonService:CommonService) { }

  ngOnInit() {
  }

  openCategories() {
    this.modal.open(this.contentCategories).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
    });
  }

  postCategory(): void {
    this.categoriesService.addCategory(this.inputCategoryName).subscribe(category => this.categoriesService.categories.push(category));
    this.inputCategoryName = "";
  }

  async deleteCategory(idCategory: number) {
    await this.categoriesService.deleteCategory(this.categoriesService.categories[idCategory].id);
    this.categoriesService.categories.splice(idCategory, 1);
  }

  showModal(type: string, data: any) {
    this.commonService.showModal$.next({ modalName: type, modalData: data });
  }

}
