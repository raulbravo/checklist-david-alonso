import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoriesService } from 'src/categories/categories-service.service';
import { Category } from 'src/models/category.model';
import { TagsService } from 'src/tags/tags-service.service';
import { TasksService } from 'src/tasks/tasks-service.service';

@Component({
  selector: 'app-addTaskModal',
  templateUrl: './addTaskModal.component.html',
  styleUrls: ['./addTaskModal.component.css']
})
export class AddTaskModalComponent implements OnInit {
  @ViewChild('content1') content1: any;
  closeResult: string;
  inputTaskText: string = "";
  inputTaskTags: string = "";
  inputTaskCategory: Category = new Category();
  tagsToAdd: string[] = [];



  constructor(public modal: NgbModal,
    public categoriesService: CategoriesService,
    public tasksService: TasksService,
    public tagsService: TagsService) { }
    
  ngOnInit(): void {

  }

  open1() {

    this.modal.open(this.content1).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      if (result === 'Save click task') {
        this.postTask();
      }
    }, (reason) => {
    });
  }

  async postTask() {
    var tagsTaskId: string[] = [];
    if (this.inputTaskTags.length > 0) {
      let arrayTags = this.inputTaskTags.split(" ");
      var arrayTagsFilter = arrayTags.filter(function (elem, index, self) {
        return index === self.indexOf(elem);
      });
      this.tagsToAdd = [];
      arrayTags.forEach((tagValide) => {
        if (this.validateTag(tagValide) == false) {
          this.tagsToAdd.push(tagValide);
        }
      });
      for (let tagAdd of this.tagsToAdd) {
        await this.postTag(tagAdd);
      }
      await this.getTags();
      for (let tagFilter of arrayTagsFilter) {
        tagsTaskId.push(this.consultIdTagTask(tagFilter));
      }
    }
    try {
      await this.tasksService.addTask(this.inputTaskText, tagsTaskId, this.inputTaskCategory.id);
    } catch (error) {
      console.log(error);
    }
    this.getTasks();
    this.inputTaskCategory = new Category();
    this.inputTaskTags = "";
    this.inputTaskText = "";
  }

  validateTag(newTag: string): boolean {
    let flagValidate: boolean = false;
    this.tagsService.tags.forEach((tagExisting) => {
      if (tagExisting.name.toLowerCase() == newTag.toLowerCase()) {
        flagValidate = true;
      }
    });
    return flagValidate;
  }

  async postTag(tagValidated: string) {
    await this.tagsService.addTag(tagValidated);
  }

  async getTags() {
    this.tagsService.tags = await this.tagsService.getAllTags().toPromise();
  }

  consultIdTagTask(tagName: string): string {
    return this.tagsService.getIdTag(tagName);
  }
  getTasks(): void {
    this.tasksService.getAllTasks().subscribe(res => {
      this.tasksService.tasks = res;
    })
  }

}
