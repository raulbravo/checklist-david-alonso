import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoriesService } from 'src/categories/categories-service.service';
import { TasksService } from 'src/tasks/tasks-service.service';

@Component({
  selector: 'app-editCategoryModal',
  templateUrl: './editCategoryModal.component.html',
  styleUrls: ['./editCategoryModal.component.css']
})
export class EditCategoryModalComponent implements OnInit {
  @ViewChild('editCategoryContent') contentEditCategory: any;
  closeResult: string;
  inputEditCategory: string = "";


  constructor(public modal: NgbModal,
    public categoriesService: CategoriesService,
    public tasksService: TasksService) { }

  ngOnInit() {
  }

  openEditCategory(data: any) {
    this.inputEditCategory = data.name;
    this.modal.open(this.contentEditCategory).result.then((result) => {
      if (result === 'Save edit category') {
        this.editCategory(data.id);
      }
    }, (reason) => {
    });
  }

  editCategory(idCategory: string) {
    this.categoriesService.editCategory(idCategory, this.inputEditCategory);
    this.categoriesService.categories.forEach((category) => {
      if (category.id === idCategory) {
        category.name = this.inputEditCategory;
      }
    })
  }

}
