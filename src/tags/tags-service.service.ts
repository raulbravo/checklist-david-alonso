import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tag } from 'src/models/tag.model';

@Injectable({
  providedIn: 'root'
})


export class TagsService {
tags:Tag[]=[];
url:string = "https://checklistbackend.azurewebsites.net/api/tags";

constructor(private http:HttpClient) { }

 getAllTags():Observable<Tag[]>{
  return  this.http.get<Tag[]>(this.url); 
}

async addTag(tagName:String):Promise<Tag>{
  return await this.http.post<Tag>(this.url, {name: tagName}).toPromise();
 }

 getIdTag(nameTag:string):string{
   
   let tagElement = this.tags.filter(tag => tag.name.toLowerCase() == nameTag.toLowerCase());
   if(tagElement.length > 0){
     return tagElement[0].id;
   }else{
     return "";
   }
}

async deleteTag(idTag: string):Promise<any>{
  let endPoints = "/" + idTag;
  return await this.http.delete(this.url + endPoints).toPromise();
}

}