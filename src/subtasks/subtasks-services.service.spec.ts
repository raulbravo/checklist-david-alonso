/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SubtasksServicesService } from './subtasks-services.service';

describe('Service: SubtasksServices', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubtasksServicesService]
    });
  });

  it('should ...', inject([SubtasksServicesService], (service: SubtasksServicesService) => {
    expect(service).toBeTruthy();
  }));
});
