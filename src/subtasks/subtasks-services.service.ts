import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SubTask } from 'src/models/subtask.model';
import { TasksService } from 'src/tasks/tasks-service.service';


@Injectable({
  providedIn: 'root'
})
export class SubtasksService {
  url: string = "https://checklistbackend.azurewebsites.net/api/tasks";
  constructor(private http: HttpClient, private taskService: TasksService) { }


  addSubTask(idTask: string, textSubtask: string, sortNumber: number): Observable<SubTask> {
    let apiURL = this.url + "/{" + idTask + "}/subtasks";
    return this.http.post<SubTask>(apiURL, { text: textSubtask, sortNumber: 0 });
  }

  deleteSubtask(idTask: string, idSubTask: string) {
    let apiURL = this.url + "/{" + idTask + "}/subtasks/{" + idSubTask + "}";
    this.http.delete(apiURL).subscribe(res => {

    });
  }

  async editSubTask(idSubTask: string, updatedValueText: string, updatedValueSortNumber: number): Promise<any> {
    let idTask: String = "";
    this.taskService.tasks.forEach((task) => {
      task.subTasks.forEach((subtask) => {
        if (subtask.id === idSubTask) {
          idTask = task.id;
        }
      })
    })

    let endPoint = this.url + "/" + idTask + "/subtasks/" + idSubTask;

    return await this.http.patch<any>(endPoint, [{ "op": "replace", "path": "text", "value": updatedValueText },
    { "op": "replace", "path": "sortNumber", "value": updatedValueSortNumber }]).toPromise();

  }

  async updateDone(subtask: SubTask, taskId: string): Promise<any> {
    let endPoint = this.url + "/" + taskId + "/subtasks/" + subtask.id;
    if (subtask.done == true) {
      subtask.done = false;
    } else {
      subtask.done = true;
    }

    return await this.http.patch<any>(endPoint, [{ "op": "replace", "path": "done", "value": subtask.done }]).toPromise();

  }

}