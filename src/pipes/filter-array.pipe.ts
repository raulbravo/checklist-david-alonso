import { Pipe, PipeTransform } from '@angular/core';
import { Task } from 'src/models/task.model';

@Pipe({
  name: 'filterArray'
})
export class FilterArrayPipe implements PipeTransform {

  transform(value: Task[], ...args: any[]): Task[] {
    const search = args[0] || '';
    const categoryName = args[2] || '';
    const tagName = args[1] || '';
    const showDone = args[3];
    return value.sort(function (a, b) { var sortNumberA = a.sortNumber; var sortNumberB = b.sortNumber; return (sortNumberA < sortNumberB) ? -1 : (sortNumberA > sortNumberB) ? 1 : 0; })
      .sort(function (a, b) { var textA = a.category.name.toUpperCase(); var textB = b.category.name.toUpperCase(); return (textA < textB) ? -1 : (textA > textB) ? 1 : 0; })
      .filter(element => element.text.toLowerCase().includes(search))
      .filter(element => element.category.name.includes(categoryName))
      .filter(element => !tagName || element.tags.find(tagElement => tagElement.name == tagName))
      .filter(element => { if (showDone == true) { return element; } else { return element.done == showDone } });
  }
}
