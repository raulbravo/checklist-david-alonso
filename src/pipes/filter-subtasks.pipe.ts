import { Pipe, PipeTransform } from '@angular/core';
import { SubTask } from 'src/models/subtask.model';

@Pipe({
  name: 'filterSubtasks'
})
export class FilterSubtasksPipe implements PipeTransform {

  transform(value: SubTask[]): SubTask[] {
    return value.sort(function (a, b) { var sortNumberA = a.sortNumber; var sortNumberB = b.sortNumber; return (sortNumberA < sortNumberB) ? -1 : (sortNumberA > sortNumberB) ? 1 : 0; });
  }

}
